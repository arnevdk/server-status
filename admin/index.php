<?php require_once("../resources/php/inc.php"); ?>


<html>
	<head>
		<title><?php echo $server_name; ?> | admin</title>

		<?php include_once("../includes/styles.inc.php") ?>

	</head>
	<body>
		<?php include_once("../includes/navbar.inc.php") ?>
		<div class="container" style="padding-top: 50px;">
			<h1><?php echo $server_name ?> admin page</h1>
			<hr>

			<div class="row">
				<div class="col-md-8">
					<h2>System info</h2>
						<table class="table">
								<tbody>
									<tr>
										<td>Operating system:</td>
										<td class="text-muted"><?php echo php_uname(); ?></td>
									</tr>
									<tr>
										<td >Processor:</td>
										<td class="text-muted"><?php echo server_cpu_info(); if(num_cpus() > 1):?> x <?php echo num_cpus(); endif; ?></td>
									</tr>
									<tr>
										<td>Memory:</td>
										<td class="text-muted"><?php echo server_ram_info(); ?></td>
									</tr>
									<tr>
										<td>HTTP Server:</td>
										<td class="text-muted"><?php echo $_SERVER['SERVER_SOFTWARE']; ?></td>
									</tr>
							</tbody>
						</table>
				</div>
				<div class="col-md-4">
					<h2>Tools</h2>
					<ul class="list-group">
					  <li class="list-group-item"><a href="phpinfo.php">phpInfo</a></li>
						<li class="list-group-item"><a href="phpmyadmin">phpMyAdmin</a></li>
						<li class="list-group-item"><a href="phpldapadmin">phpLDAPAdmin</a></li>
					</ul>
				</div>


			</div>
			<hr>
			<h2>Status</h2>
			<div class="row">
				<div class="col-md-6">
					<?php foreach($disks as $disk): ?>
				  <h4>Disk root: <em><?php echo $disk; ?></em></h4>
          <?php if (check_disk($disk)): ?>
  					<span class="text-muted"><?php echo disk_used($disk); ?> MiB of <?php echo disk_total($disk) ?> MiB used</span>
  					<div class="progress">
  						<div class="progress-bar" role="progressbar" aria-valuenow="<?php echo disk_used($disk); ?>" aria-valuemin="0" aria-valuemax="<?php echo disk_total($disk); ?>" style="width:<?php echo disk_percentage($disk); ?>%">
      							<span class="sr-only"><?php echo $used ?> MB used</span>
    						</div>
  					</div>
          <?php else: ?>
              <strong>Disk not found or no permissions.</strong>
              <div class="progress">
    						<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0">
        							<span class="sr-only">0 MB used</span>
      						</div>
    					</div>
          <?php endif; ?>
					<?php endforeach; ?>
				</div>
				<div class="col-md-6">
					<h4>RAM usage</h4>
					<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<canvas id="ram-chart" style="width: 50%"></canvas>
					</div>
					</div>
					 <h4>CPU usage</h4>

					<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<canvas id="cpu-chart" style="width: 50%"></canvas>
					</div>
					</div>
				</div>
			</div>
			<hr>
			<h2>Services</h2>
			<?php include("../includes/servicelist_admin.inc.php"); ?>
		</div>
		<?php include("../includes/footer.inc.php"); ?>
	</body>


	<script>
  	var ctx = document.getElementById("cpu-chart").getContext('2d');
  	var cpu_chart = new Chart(ctx, {
      		type: 'doughnut',
      		data: {
        		labels: ["Used", "Free"],
        		datasets: [
          			{
  	  			label: "CPU usage",
            			data: [<?php echo get_server_cpu_usage(); ?>,<?php echo  get_server_max_cpu() - get_server_cpu_usage(); ?>],
            			backgroundColor: ['#337ab7', Chart.defaults.global.defaultColor]
  				}
        			]
      		}
  	});

  	ctx = document.getElementById("ram-chart").getContext('2d');
          var ram_chart = new Chart(ctx, {
                  type: 'doughnut',
                  data: {
                  labels: ["Used", "Free"],
                  datasets: [
                                  {
                                  label: "RAM usage",
                                  data: [<?php echo get_server_ram_usage(); ?>,<?php echo get_server_total_ram() - get_server_ram_usage(); ?>],
                                  backgroundColor: ['#337ab7', Chart.defaults.global.defaultColor]
                                  }
                          ]
                  }
          });

	</script>


</html>
